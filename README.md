# Batt

A simple app that shows battery status information on Android devices. **Requires Android 14.**

<img height="800" src="screenshot.png"></img>

## Installation

Install it from the [latest release](https://gitlab.com/narektor/batt/-/releases).

## What Batt displays

By default, it shows:
- the number of charge cycles
- the charge status

However, if the `android.permission.BATTERY_STATS` permission is granted (which can be done from the app itself if you have [Shizuku](https://shizuku.rikka.app)), it can also show:
- the battery health
- the battery manufacturing date
- the battery first use date

## Technical

The app is built using the traditional View-based system and Material 3 components. To get the `BATTERY_STATS` permission it uses the [Shizuku API](github.com/RikkaApps/Shizuku-API).

Make sure to use Android Studio Giraffe or later.

## Acknowledgements
- [Android 14 API Tests](https://gitlab.com/android-api-tests/upside-down-cake) for the base
- [Mishaal Rahman](https://t.me/MishaalAndroidNews/490) for finding and reporting on the new APIs
